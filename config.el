;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!


;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets.
(setq user-full-name "Davide Peressoni"
      user-mail-address "davide.peressoni@tuta.io")

;; Doom exposes five (optional) variables for controlling fonts in Doom. Here
;; are the three important ones:
;;
;; + `doom-font'
;; + `doom-variable-pitch-font'
;; + `doom-big-font' -- used for `doom-big-font-mode'; use this for
;;   presentations or streaming.
;;
;; They all accept either a font-spec, font string ("Input Mono-12"), or xlfd
;; font string. You generally only need these two:
;; (setq doom-font (font-spec :family "monospace" :size 12 :weight 'semi-light)
;;       doom-variable-pitch-font (font-spec :family "sans" :size 13))
(setq  doom-font (font-spec :family "Hasklig" :size 13) ; :weight 'light)
       doom-big-font (font-spec :family "Hasklig" :size 24))



;; BUG (?) in emacs deamon
;; https://stackoverflow.com/questions/8204316/cant-change-cursor-color-in-emacsclient
(setq evil-default-cursor t)
;; (set-cursor-color "RoyalBlue")
;; (set-mouse-color "RoyalBlue")

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. This is the default:
        (setq doom-theme 'doom-gruvbox-light) ; 'doom-opera-light)

;; If you use `org' and don't want your org files in the default location below,
;; change `org-directory'. It must be set before org loads!
(setq org-directory "~/Documenti/NextCloud/Org/")

;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
(setq display-line-numbers-type t)


;; Here are some additional functions/macros that could help you configure Doom:
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package!' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c c k').
;; This will open documentation for it, including demos of how they are used.
;;
;; You can also try 'gd' (or 'C-c c d') to jump to their definition and see how
;; they are implemented.

;; (add-hook 'window-setup-hook #'treemacs 'append)

;; Start maximized
(add-to-list 'default-frame-alist '(fullscreen . maximized))
;; ** Don't ask to quit
(setq confirm-kill-emacs nil)

;; Frame title
(setq frame-title-format
      '(""
        (:eval
         (if nil ;(s-contains-p org-roam-directory (or buffer-file-name ""))
             (replace-regexp-in-string
              ".*/[0-9]*-?" "☰ "
              (subst-char-in-string ?_ ?  buffer-file-name))
           "%b"))
        (:eval
         (let ((project-name (projectile-project-name)))
           (unless (string= "-" project-name)
             (format (if (buffer-modified-p)  " ◉ %s" "  ●  %s") project-name))))))

;; Another configs
(setq-default
 delete-by-moving-to-trash t                      ; Delete files to trash
 window-combination-resize t                      ; take new window space from all other windows (not just current)
 x-stretch-cursor t)                              ; Stretch cursor to the glyph width

(setq undo-limit 80000000                         ; Raise undo-limit to 80Mb
      evil-want-fine-undo t                       ; By default while in insert all changes are one big blob. Be more granular
      auto-save-default t                         ; Nobody likes to loose work, I certainly don't
      truncate-string-ellipsis "…"                ; Unicode ellispis are nicer than "...", and also save /precious/ space
      password-cache-expiry nil                   ; I can trust my computers ... can't I?
      ;; scroll-preserve-screen-position 'always  ; Don't have `point' jump around
      scroll-margin 2                             ; It's nice to maintain a little margin

      evil-cross-lines t
      global-visual-line-mode t

      doom-modeline-icon t ;(display-graphic-p) ; Display icons in GUI but not in TUI
      doom-modeline-major-mode-icon t ;(display-images-p)
      ;; doom-modeline-buffer-encoding t
      doom-modeline-indent-info t
      +format-on-save-enabled-modes t
      )

;;BEGIN local variable
(setq enable-local-variables t                    ; Ask if to accpet not safe file-local-variables
      safe-local-variable-values '((org-duration-format . h:mm)
                                   (+format-with-lsp . nil))
      ;;       safe-local-eval-forms '((add-hook 'after-change-major-mode-hook #'format-all-mode)
      ;;                               (add-hook 'after-change-major-mode-hook (lambda () (add-hook 'before-save-hook #'+format/buffer)))
      ;;                               (add-hook 'rustic-mode-hook (lambda () (setq rustic-indent-offset 4))))
      )
;; (put 'indent-tabs-mode 'safe-local-variable #'booleanp)
;; (put 'tab-width 'safe-local-variable #'numberp)
;; (put 'rustic-indent-offset 'safe-local-variable #'numberp)
(defvar my-trusted-paths
  '("/home/dpd-/Documenti/NextCloud/Org/"
    "/home/dpd-/.dotfiles/"
    "/home/dpd-/Documenti/ProgrammazioneRust/"))
(defvar my-trusted-paths-prefix
  '("/home/dpd-/SVILUPPO/"))

(defun my-trusted-paths-filter (dir-name)
  (catch 'result
    (dolist (p my-trusted-paths)
      (when (string-equal p dir-name)
        (throw 'result t)))
    (dolist (p my-trusted-paths-prefix)
      (when (string-prefix-p p dir-name)
        (throw 'result t)))))

(advice-add 'hack-local-variables-filter
            :around
            #'
            (lambda (old-fn &rest args)
              (pcase-let ((`(,_variables ,dir-name) args))
                (if (my-trusted-paths-filter dir-name)
                    (cl-letf (((symbol-function 'safe-local-variable-p) (lambda (&rest _) t)))
                      (apply old-fn args)
                      ;; Always accept paths matching.
                      t)
                  (apply old-fn args)))))
;;END local variable

;; (after! treemacs
;;   (setq treemacs-width 20
;;         treemacs-width-is-initially-locked nil))
(after! dired-sidebar
  (setq dired-sidebar-subtree-line-prefix " "
        dired-sidebar-one-instance-p t)
  )
(map! :leader :desc "Toggle sidebar" "o p" 'dired-sidebar-toggle-sidebar)

;; Zero-width (invisible) space
(map! :map org-mode-map
      :nie "M-SPC M-SPC" (cmd! (insert "\u200B")))
(defun +org-export-remove-zero-width-space (text _backend _info)
  "Remove zero width spaces from TEXT."
  (unless (org-export-derived-backend-p 'org)
    (replace-regexp-in-string "\u200B" "" text)))
(after! ox
  (add-to-list 'org-export-filter-final-output-functions #'+org-export-remove-zero-width-space t))

(after! lsp-mode
  (setq
   lsp-eslint-package-manager 'pnpm
   lsp-rust-clippy-preference 'on
   lsp-rust-analyzer-cargo-watch-command "clippy"))

;; Avoid projectile in home
(after! projectile
  (setq projectile-ignored-projects '("~/" "/tmp" "~/.emacs.d/.local/straight/repos/")))

(after! avy
  (setq avy-all-windows t))

(add-hook 'server-after-make-frame-hook (lambda () (setq centaur-tabs-set-icons t)(centaur-tabs-mode))) ; avoid bugs with daemon mode
(after! centaur-tabs
  (setq
   centaur-tabs-height 20
   centaur-tabs-set-modified-marker t
   centaur-tabs-style "wave"
   centaur-tabs-set-icons t)
  (centaur-tabs-group-by-projectile-project))

(after! flycheck
  (push 'rustic-clippy flycheck-checkers)
  (setq rustic-flycheck-clippy-params "--message-format=json"))

(global-subword-mode 1)                           ; Iterate through CamelCase words

(map! :n [mouse-8] #'better-jumper-jump-backward ; back/forward with mouse
      :n [mouse-9] #'better-jumper-jump-forward)


;; WORD WRAP
(+global-word-wrap-mode +1)
(add-hook 'before-save-hook
          'delete-trailing-whitespace)
;; move across wrapped line
(map! :after evil-org
      :map evil-org-mode-map
      :nv "gj" 'evil-next-visual-line
      :nv "gk" 'evil-previous-visual-line)
;; (define-key evil-normal-state-map (kbd "<remap> <evil-next-line>") 'evil-next-visual-line)
;; (define-key evil-normal-state-map (kbd "<remap> <evil-previous-line>") 'evil-previous-visual-line)
;; (define-key evil-motion-state-map (kbd "<remap> <evil-next-line>") 'evil-next-visual-line)
;; (define-key evil-motion-state-map (kbd "<remap> <evil-previous-line>") 'evil-previous-visual-line)

;; START TABS CONFIG
;; make indent commands use space only (never tab character)
(setq-default indent-tabs-mode nil)
(add-hook 'prog-mode-hook 'highlight-indent-guides-mode)
(setq-default standard-indent 2
              tab-width standard-indent
              sh-indentation standard-indent
              tex-indent-arg standard-indent)
(after! rustic
  (setq rustic-indent-offset standard-indent))
(after! typescript
  (setq typescript-indent-level standard-indent))
(after! markdown-mode
  (setq markdown-list-indent-width standard-indent))
;; END TABS CONFIG

;; START WHITESPACE
;; (use-package! whitespace
;;   :config
;;   (setq
;;    whitespace-style '(
;;                       face ;background color
;;                       spaces
;;                       ;;whitespace-trailing
;;                       tabs
;;                       ;; newline
;;                       space-mark
;;                       ;;whitespace-trailing-mark
;;                       tab-mark
;;                       ;; newline-mark
;;                       )
;;    whitespace-display-mappings
;;    ;; all numbers are Unicode codepoint in decimal. ⁖ (insert-char 182 1)
;;    '(
;;      (space-mark 32 [183] [46]) ; 32 SPACE 「 」, 183 MIDDLE DOT 「·」, 46 FULL STOP 「.」
;;      (newline-mark 10 [182 10]) ; 10 LINE FEED
;;      (tab-mark 9 [9655 9] [92 9]) ; 9 TAB, 9655 WHITE RIGHT-POINTING TRIANGLE 「▷」
;;      )
;;    )
;;   (global-whitespace-mode +1)
;;  )
;; (set-face-attribute 'whitespace-space nil :background nil :foreground "gray30")
;; END WHITESPACE


;;BEGIN ligatures
(after! rustic
  (set-ligatures! 'rustic-mode
    ;; Functional
    :def           "fn"
    :map           "map"
    :yield         "use"
    ;; Types
    :null          "None"
    :true          "true"
    :false         "false"
    :bool          "bool"
    :int           "i32"
    :float         "f64"
    :string        "String"
    ;; Flow
    :in            "in"
    :and           "&&"
    :or            "||"
                                        ;:not           "!" ; possible conflict with macros
    :for           "for"
    :some          "Some"
    :return        "return"))
(after! js
  (set-ligatures! 'js-mode
    ;; Functional
    :def           "function"
    :lambda        "()=>"
    :lambda        "() =>"
    :lambda        "function()"
    :lambda        "function ()"
    :map           "map"
    ;; Types
    :null          "null"
    :true          "true"
    :false         "false"
    :bool          "bool"
    :float         "number"
    :string        "string"
    ;; Flow
    :in            "in"
    :and           "&&"
    :or            "||"
    :not           "!"
    :for           "for"
    :for           "forEach"
    :yield         "yield"
    :return        "return"))
(after! cc-mode
  (set-ligatures! 'java-mode
    ;; Functional
    :map           "map"
    ;; Types
    :null          "null"
    :true          "true"
    :false         "false"
    :bool          "boolean"
    :int           "int"
    :float         "float"
    :string        "String"
    ;; Flow
    :and           "&&"
    :or            "||"
    :not           "!"
    :for           "for"
    :yield         "yield"
    :return        "return"))
;;END ligatures

;; BUG (?) in emacs deamon
;; https://stackoverflow.com/questions/8204316/cant-change-cursor-color-in-emacsclient
;; (setq evil-default-cursor t)
;; (set-cursor-color "RoyalBlue")
;; (set-mouse-color "RoyalBlue")
